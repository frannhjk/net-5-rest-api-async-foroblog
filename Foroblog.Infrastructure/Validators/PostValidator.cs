﻿using FluentValidation;
using Foroblog.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Foroblog.Infrastructure.Validators
{
    public class PostValidator : AbstractValidator<PostDTO>
    {
        public PostValidator()
        {
            RuleFor(post => post.Description)
                .NotNull()
                .Length(10, 500);

            RuleFor(post => post.Date)
                .NotNull()
                .GreaterThan(DateTime.Now);
        }
    }
}
