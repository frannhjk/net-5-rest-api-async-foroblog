﻿using AutoMapper;
using Foroblog.Core.DTOs;
using Foroblog.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Foroblog.Infrastructure.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Post, PostDTO>()
                .ForMember(destination => destination.PostId, opts => opts.MapFrom(src => src.Id));
            CreateMap<PostDTO, Post>();

            CreateMap<SecurityDTO, Security>();
            CreateMap<Security, SecurityDTO>();
        }
    }
}
