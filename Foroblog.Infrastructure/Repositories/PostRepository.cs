﻿using Foroblog.Core.Entities;
using Foroblog.Core.Interfaces;
using Foroblog.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foroblog.Infrastructure.Repositories
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        public PostRepository(ForoblogContext context) : base(context) { }

        public async Task<IEnumerable<Post>> GetPostsByUser(int userId)
        {
            return await base._entities.Where(x => x.UserId == userId)
                .ToListAsync();
        }
    }
}
