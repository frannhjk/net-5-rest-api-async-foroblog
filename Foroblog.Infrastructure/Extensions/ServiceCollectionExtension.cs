﻿using AutoMapper.Configuration;
using Foroblog.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Foroblog.Core.CustomEntities;
using Foroblog.Core.Interfaces;
using Foroblog.Core.Services;
using Foroblog.Infrastructure.Repositories;
using Foroblog.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Http;
using Foroblog.Infrastructure.Services;

namespace Foroblog.Infrastructure.Extensions
{
    public static class ServiceCollectionExtension
    {
        // Utilizo return services, en lugar de void para poder "Encadenar" métodos.
        // ejemplo:       
                            //services.AddOptionsExtension(Configuration)
                            //    .AddDbContextExtension(Configuration);
        // This para el método que debemos extender

        public static IServiceCollection AddDbContextExtension(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            // Configure DbContext
            services.AddDbContext<ForoblogContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("Foroblog"))
            );

            return services;
        }

        public static IServiceCollection AddOptionsExtension(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            // Set by DI (Singleton) PaginationOptions class properties from Pagination section in AppSettings file
            services.Configure<PaginationOptions>(options => configuration.GetSection("Pagination").Bind(options));

            return services;
        }

        public static IServiceCollection AddServicesExtension(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));

            services.AddTransient<IPostService, PostService>();
            services.AddTransient<ISecurityService, SecurityService>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddSingleton<IPasswordService, PasswordService>();
            services.AddSingleton<IUriService>(provider =>
            {
                var accesor = provider.GetRequiredService<IHttpContextAccessor>();
                var request = accesor.HttpContext.Request; // Get request from client
                var absoluteUri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent());
                return new UriService(absoluteUri);
            });

            return services;
        }
    }
}
