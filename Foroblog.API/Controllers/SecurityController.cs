﻿using AutoMapper;
using Foroblog.API.Responses;
using Foroblog.Core.DTOs;
using Foroblog.Core.Entities;
using Foroblog.Core.Enums;
using Foroblog.Core.Interfaces;
using Foroblog.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foroblog.API.Controllers
{
    /// <summary>
    /// Only Admin User can create new users
    /// </summary>
    //[Authorize(Roles = nameof(RoleType.Administrador))]
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private readonly ISecurityService _securityService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        private readonly IPasswordService _passwordService;

        public SecurityController(ISecurityService securityService, IMapper mapper, IUriService uriService, IPasswordService passwordService)
        {
            _securityService = securityService;
            _mapper = mapper;
            _uriService = uriService;
            _passwordService = passwordService;
        }

        [HttpPost]
        public async Task<IActionResult> RegisterUser(SecurityDTO securityDTO)
        {
            try
            {
                // Si existe el UserName, utilizo BadRequest para no revelar información
                if (await _securityService.ExistingUser(securityDTO.UserName))
                    return BadRequest();
                
                var securityEntity = _mapper.Map<Security>(securityDTO);

                // Hashing password
                securityEntity.Password = _passwordService.Hash(securityEntity.Password);

                await _securityService.RegisterUser(securityEntity);

                var resultDTO = _mapper.Map<SecurityDTO>(securityEntity);

                var response = new ApiResponse<SecurityDTO>(resultDTO);

                return Ok(response);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString()); 
            }
        }
    }
}
