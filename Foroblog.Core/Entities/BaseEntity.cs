﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Foroblog.Core.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
