﻿using System;
using System.Collections.Generic;

namespace Foroblog.Core.Entities
{
    public partial class Comment : BaseEntity
    {
        public int PostId { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public bool Active { get; set; }

        public virtual Post Post { get; set; }
        public virtual User User { get; set; }
    }
}
