﻿using Foroblog.Core.CustomEntities;
using Foroblog.Core.Entities;
using Foroblog.Core.QueryFilter;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Foroblog.Core.Interfaces
{
    public interface IPostService
    {
        PagedList<Post> GetPostsAsync(PostQueryFilter filters);
        Task<Post> GetPostAsync(int id);
        Task InsertPost(Post post);
        Task<bool> UpdatePost(Post post);
        Task<bool> DeletePost(int id);
    }
}