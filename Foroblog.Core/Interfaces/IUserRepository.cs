﻿using Foroblog.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Foroblog.Core.Interfaces
{
    public interface IUserRepository
    {
        Task<User> GetUserAsync(int id);
        Task<IEnumerable<User>> GetUsersAsync();
    }
}