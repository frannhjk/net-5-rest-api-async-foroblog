﻿using Foroblog.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Foroblog.Core.Interfaces
{
    // Generic Repository Pattern (Restricción: Para poder utlizarlo debe ser un tipo entidad "T" que herede de BaseEntity (Modelo))
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        Task<List<T>> GetAllAsync();
        Task<T> GetById(int id);
        Task Add(T entity);
        void Update(T entity);
        Task Delete(int id);
    }
}
