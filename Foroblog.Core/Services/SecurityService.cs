﻿using Foroblog.Core.Entities;
using Foroblog.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foroblog.Core.Services
{
    public class SecurityService : ISecurityService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SecurityService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Security> GetLoginByCredentials(UserLogin login)
        {
            return await _unitOfWork.SecurityRepository.GetLoginByCredentials(login);
        }

        public async Task RegisterUser(Security security)
        {
            await _unitOfWork.SecurityRepository.Add(security);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> ExistingUser(string userName)
        {
            //var users = _unitOfWork.SecurityRepository.GetAll()
            //    .Where(user => user.UserName == userName)
            //    .ToList();

            var usersAsync = await _unitOfWork.SecurityRepository.GetAllAsync();

            var users = usersAsync
                .Where(user => user.UserName == userName)
                .ToList();

            return users.Count > 0 ? true : false;
        }
    }
}
