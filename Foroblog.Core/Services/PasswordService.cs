﻿using Foroblog.Core.Interfaces;
using Foroblog.Core.Options;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace Foroblog.Core.Services
{
    public class PasswordService : IPasswordService
    {
        public PasswordService()
        {

        }

        public bool Check(string loginPassword, string userPassword)
        {
            return BCrypt.Net.BCrypt.Verify(loginPassword, userPassword);
        }

        public string Hash(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }
    }
}
