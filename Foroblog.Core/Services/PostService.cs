﻿using Foroblog.Core.CustomEntities;
using Foroblog.Core.Entities;
using Foroblog.Core.Exceptions;
using Foroblog.Core.Interfaces;
using Foroblog.Core.QueryFilter;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foroblog.Core.Services
{
    /// <summary>
    /// 1. Sólo se permite publicación a un usuario previamente registrado
    /// 2. Si el usuario tiene menos de 10 publicaciones, solo puede publicar 1 sola vez por semana
    /// 3. No es permitido publicar información que haga referencia a Sexo
    /// </summary>

    public class PostService : IPostService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly PaginationOptions _paginationOptions;

        public PostService(IUnitOfWork unitOfWork, IOptions<PaginationOptions> options)
        {
            _unitOfWork = unitOfWork;
            _paginationOptions = options.Value;
        }

        public PagedList<Post> GetPostsAsync(PostQueryFilter filters)
        {
            filters.PageNumber = filters.PageNumber == 0 ? _paginationOptions.DefaultPageNumber : filters.PageNumber;
            filters.PageSize = filters.PageSize == 0 ? _paginationOptions.DefaultPageSize : filters.PageSize;

            var posts = _unitOfWork.PostRepository.GetAll();

            if (filters.UserId != null)
            {
                posts = posts.Where(post => post.UserId == filters.UserId);
            }

            if (filters.Date != null)
            {
                posts = posts.Where(post => post.Date.ToShortDateString() == filters.Date?.ToShortDateString());
            }

            if (filters.Description != null)
            {
                posts = posts.Where(post => post.Description.ToLower().Contains(filters.Description.ToLower()));
            }

            // Pagination
            var pagedPost = PagedList<Post>.Create(posts, filters.PageNumber, filters.PageSize);

            return pagedPost;
        }

        public async Task<Post> GetPostAsync(int id)
        {
            return await _unitOfWork.PostRepository.GetById(id);
        }

        public async Task InsertPost(Post post)
        {
            var user = await _unitOfWork.UserRepository.GetById(post.UserId);

            // ** Solo se permite publicación de un usuario registrado ** 
            if (user is null)
                throw new BusinessException("User does not exists");

            // ** Si el usuario tiene menos de 10 publicaciones, solo puede postear 1 vez por semana
            var userPosts = await _unitOfWork.PostRepository.GetPostsByUser(post.UserId);
            if (userPosts.Count() < 10)
            {
                var lastPostDate = userPosts.LastOrDefault();
                if ((DateTime.Now - lastPostDate.Date).TotalDays < 7)
                    throw new BusinessException("You are not able to publish the post");    
            }
           
            // ** No es permitido publicar información que haga referencia a Sexo **
            if (post.Description.Contains("Sexo"))
                throw new BusinessException("Post cannot contains sex references");

            await _unitOfWork.PostRepository.Add(post);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> UpdatePost(Post post)
        {
            var existingPost = await _unitOfWork.PostRepository.GetById(post.Id);

            existingPost.Image = post.Image;
            existingPost.Description = post.Description;

            _unitOfWork.PostRepository.Update(post);

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeletePost(int id)
        {
            await _unitOfWork.PostRepository.Delete(id);

            await _unitOfWork.SaveChangesAsync();
             
            return true;
        }
    }
}
